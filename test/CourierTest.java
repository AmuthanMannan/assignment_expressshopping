import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class CourierTest {

    @Test
    public void shouldAbleToAddCategoryChargersInCourier(){
        Courier courier=new Courier(CourierName.BLUEDART);
        CategoryCharger categoryChargerOne=new CategoryCharger(10,0,0);
        CategoryCharger categoryChargerTwo=new CategoryCharger(20,10,0);
        HashMap<CategoryName,CategoryCharger> charges=new HashMap<>();
        charges.put(CategoryName.FITNESS,categoryChargerOne);
        charges.put(CategoryName.HOME_APPLIANCES,categoryChargerTwo);
        courier.setCategoryChargers(charges);
        Assert.assertEquals(new CategoryCharger(10,0,0),courier.getCategoryCharger(CategoryName.FITNESS));
    }

    @Test
    public void shouldAbleToAddPlaceChargersInCourier(){

        Courier courier=new Courier(CourierName.FEDEX);
        PlaceCharger placeChargerOne=new PlaceCharger(10);
        PlaceCharger placeChargerTwo=new PlaceCharger(30);
        HashMap<PlaceName,PlaceCharger> charges=new HashMap<>();
        charges.put(PlaceName.BENGALORE,placeChargerOne);
        charges.put(PlaceName.CHENNAI,placeChargerTwo);
        courier.setPlaceChargers(charges);
        Assert.assertEquals(new PlaceCharger(30),courier.getPlaceCharger(PlaceName.CHENNAI));

    }
}
