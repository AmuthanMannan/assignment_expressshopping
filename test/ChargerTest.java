import org.junit.Assert;
import org.junit.Test;

public class ChargerTest {

    @Test
    public void shouldAbleToChargeForSpecificCategory(){
        Price price=new Price(100);
        CategoryCharger categoryCharger=new CategoryCharger(50,0,0);
        categoryCharger.charge(price);
        Assert.assertEquals(new Price(150),price);
    }

    @Test
    public void shouldAbleToChargeForSpecificCategoryWithFlatExtras(){
        Price price=new Price(100);
        CategoryCharger categoryCharger=new CategoryCharger(50,50,0);
        categoryCharger.charge(price);
        Assert.assertEquals(new Price(200),price);
    }

    @Test
    public void shouldAbleToChargeForSpecificPlace(){
        Price price=new Price(100);
        CategoryCharger categoryCharger=new CategoryCharger(50,50,0);
        categoryCharger.charge(price);
        Assert.assertEquals(new Price(200),price);
    }

    @Test
    public void shouldAbleToChargeForCategoryAndPrice(){
        Price price=new Price(100);
        CategoryCharger categoryCharger=new CategoryCharger(20,0,0);
        PlaceCharger placeCharger=new PlaceCharger(20);
        categoryCharger.charge(price);
        placeCharger.charge(price);
        Assert.assertEquals(new Price(144),price);
    }
}
