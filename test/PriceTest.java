import org.junit.Assert;
import org.junit.Test;

public class PriceTest {
    @Test
    public void shouldAbleToAddExtrasInPrice(){
        Price price=new Price(100);
        price.extraPercentage(20);
        Assert.assertEquals(new Price(120),price);
    }

    @Test
    public void shouldAbleToAddFlatExtrasInPrice(){
        Price price=new Price(100);
        price.flatExtra(100);
        Assert.assertEquals(new Price(200),price);
    }

    @Test
    public void shouldAbleToAddBothExtrasAndFlatExtrasInPrice(){
        Price price=new Price(100);
        price.extraPercentage(1);
        price.flatExtra(50);
        Assert.assertEquals(new Price(151),price);
    }

    @Test
    public void shouldAbleToDiscountThePrice(){
        Price price=new Price(100);
        price.discount(50);
        Assert.assertEquals(new Price(50),price);
    }
}
