public class Product {
    private Products name;
    private CategoryName category;
    private Price price;

    public Product(Products name, CategoryName category, Price price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }

    public Products getName() {
        return name;
    }

    public CategoryName getCategory() {
        return category;
    }

    public Price getPrice() {
        return price;
    }
}
