import java.util.Objects;

public class Price implements Comparable<Price>{
    private float amount;
    Price(float amount){
        this.amount=amount;
    }

    void extraPercentage(float percentage){
        float extra=(percentage/100f)*amount;
        amount=amount+extra;
    }
    void flatExtra(float extras){
         amount+=extras;
    }
    public float getAmount() {
        return amount;
    }
    void discount(float discount){
        float reduction=(discount/100f)*amount;
        amount-=reduction;
    }

    @Override
    public int compareTo(Price o) {
        if(o.getAmount()==this.getAmount()){
            return 0;
        }
        if(o.getAmount()>this.getAmount()){
            return -1;
        }
        return 1;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Float.compare(price.amount, amount) == 0;
    }


}
