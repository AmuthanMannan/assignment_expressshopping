import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;

public class Order {
    Price price;
    CategoryCharger categoryCharger;
    PlaceCharger placeCharger;

    public Order(Price price, CategoryCharger categoryCharger, PlaceCharger placeCharger) {
        this.price = new Price(price.getAmount());
        this.categoryCharger = categoryCharger;
        this.placeCharger = placeCharger;
    }



    Price getDelivered(){
        categoryCharger.charge(price);
        placeCharger.charge(price);
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(price, order.price) &&
                Objects.equals(categoryCharger, order.categoryCharger) &&
                Objects.equals(placeCharger, order.placeCharger);
    }
}
