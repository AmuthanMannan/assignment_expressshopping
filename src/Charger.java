public abstract class Charger {
    float extras;

    Charger(float extras){
        this.extras=extras;
    }

    abstract void charge(Price currentPrice);

}
