import java.util.HashMap;

public class Courier {
    CourierName name;
    HashMap<CategoryName,CategoryCharger> categoryChargers;
    HashMap<PlaceName,PlaceCharger> placeChargers;

    Courier(CourierName name){
        this.name=name;
    }
    void setCategoryChargers(HashMap<CategoryName,CategoryCharger> categoryChargers){
        this.categoryChargers =categoryChargers;
    }

    public void setPlaceChargers(HashMap<PlaceName,PlaceCharger>  placeChargers) {
        this.placeChargers =placeChargers;
    }

    CategoryCharger getCategoryCharger(CategoryName category){
        return categoryChargers.get(category);
    }

    PlaceCharger getPlaceCharger(PlaceName place){
        return placeChargers.get(place);
    }

}
