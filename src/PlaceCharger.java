public class PlaceCharger extends Charger {


    public PlaceCharger(float extras) {
        super(extras);
    }


    @Override
    void charge(Price currentPrice) {
        currentPrice.extraPercentage(extras);
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        PlaceCharger that = (PlaceCharger) obj;
        return Float.compare(this.extras,that.extras)==0;
    }
}
