import java.util.Objects;

public class CategoryCharger extends Charger {
    float earlyTimeExtras;
    float flatExtras;

    CategoryCharger(float extra,float flatExtras,float earlyTimeExtras){
        super(extra);
        this.flatExtras=flatExtras;
        this.earlyTimeExtras=earlyTimeExtras;
    }

    @Override
    void charge(Price currentPrice) {
        currentPrice.extraPercentage(extras);
        currentPrice.extraPercentage(earlyTimeExtras);
        currentPrice.flatExtra(flatExtras);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryCharger that = (CategoryCharger) o;
        return Float.compare(that.earlyTimeExtras, earlyTimeExtras) == 0 &&
                Float.compare(that.flatExtras, flatExtras) == 0;
    }

}
