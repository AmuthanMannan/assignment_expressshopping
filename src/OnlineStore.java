import java.util.HashMap;

public class OnlineStore {
    HashMap<Products,Product> products;
    HashMap<CourierName,Courier> couriers;
    ShoppingAid aid;

    public OnlineStore(HashMap<Products, Product> products, HashMap<CourierName, Courier> couriers) {
        this.products = products;
        this.couriers = couriers;
        aid=new ShoppingAid(couriers,products);
    }

    public OnlineStore(){
        HashMap<CategoryName,CategoryCharger> fedexCategoryCharges=new HashMap<>();
        fedexCategoryCharges.put(CategoryName.ELECTRONICS,new CategoryCharger(0,50,30));
        fedexCategoryCharges.put(CategoryName.FITNESS,new CategoryCharger(10,0,0));
        fedexCategoryCharges.put(CategoryName.HOME_APPLIANCES,new CategoryCharger(0,0,0));

        HashMap<PlaceName,PlaceCharger>fedexPlacesCharges=new HashMap<>();
        fedexPlacesCharges.put(PlaceName.CHENNAI,new PlaceCharger(20));
        fedexPlacesCharges.put(PlaceName.BENGALORE,new PlaceCharger(15));

        HashMap<CategoryName,CategoryCharger> bluedartCategoryCharges=new HashMap<>();
        bluedartCategoryCharges.put(CategoryName.ELECTRONICS,new CategoryCharger(20,0,0));
        bluedartCategoryCharges.put(CategoryName.FITNESS,new CategoryCharger(0,0,0));
        bluedartCategoryCharges.put(CategoryName.HOME_APPLIANCES,new CategoryCharger(0,0,0));

        HashMap<PlaceName,PlaceCharger>bluedartPlacesCharges=new HashMap<>();
        bluedartPlacesCharges.put(PlaceName.CHENNAI,new PlaceCharger(10));
        bluedartPlacesCharges.put(PlaceName.BENGALORE,new PlaceCharger(5));



        Courier fedex=new Courier(CourierName.FEDEX);
        fedex.setCategoryChargers(fedexCategoryCharges);
        fedex.setPlaceChargers(fedexPlacesCharges);

        Courier bluedart=new Courier(CourierName.BLUEDART);
        bluedart.setCategoryChargers(bluedartCategoryCharges);
        bluedart.setPlaceChargers(bluedartPlacesCharges);

        HashMap<CourierName,Courier> couriers=new HashMap<>();
        couriers.put(CourierName.FEDEX,fedex);
        couriers.put(CourierName.BLUEDART,bluedart);

        HashMap<Products,Product> products=new HashMap<>();
        products.put(Products.EARPHONES,new Product(Products.EARPHONES, CategoryName.ELECTRONICS,new Price(300f)));
        products.put(Products.SHOES,new Product(Products.SHOES, CategoryName.FITNESS,new Price(700f)));
        products.put(Products.IRONBOX,new Product(Products.IRONBOX, CategoryName.HOME_APPLIANCES,new Price(1200f)));

        this.products=products;
        this.couriers=couriers;
        aid=new ShoppingAid(couriers,products);
    }
    public Order getOrder(Products productName, CourierName courierService, PlaceName place){
        Order order;
        Product product=products.get(productName);
        CategoryCharger categoryCharger=couriers.get(courierService).getCategoryCharger(product.getCategory());
        PlaceCharger placeCharger=couriers.get(courierService).getPlaceCharger(place);
        order=new Order(new Price(product.getPrice().getAmount()),categoryCharger,placeCharger);
        return order;
    }

    public ShoppingAid getAid(){
        return aid;
    }








}
