import java.util.HashMap;

public class ShoppingAid {
    HashMap<CourierName, Courier> couriers;
    HashMap<Products, Product> products;
    Price choosedPrice;
    CourierName choosedService;
    public ShoppingAid(HashMap<CourierName, Courier> couriers, HashMap<Products,Product> products) {
        this.couriers=couriers;
        this.products=products;
    }

    Price getEstimationForGivenCourierService(CourierName courierService, Products productName, PlaceName place){
        Product product=products.get(productName);
        Price price=new Price(product.getPrice().getAmount());
        CategoryCharger categoryCharger=couriers.get(courierService).getCategoryCharger(product.getCategory());
        PlaceCharger placeCharger=couriers.get(courierService).getPlaceCharger(place);
        categoryCharger.charge(price);
        placeCharger.charge(price);
        return price;


    }

    void getLeastPriceAndServiceFor(Products productName, PlaceName place){

        float leastPrice=Float.MAX_VALUE;
        choosedService=null;

        for(CourierName key:couriers.keySet()){
            Price price=getEstimationForGivenCourierService(key,productName,place);
            if(leastPrice>price.getAmount()){leastPrice=price.getAmount();choosedService=key;}
        }

        choosedPrice=new Price(leastPrice);
    }

    void getLeastPriceAndServiceFor(Products[] productNames, PlaceName[] places){
        int namesSize=productNames.length;
        int placesSize=places.length;
        CourierName service= CourierName.BLUEDART;
        float priceamount=Float.MAX_VALUE;

        for(int i=0;i<namesSize;i++){
            getLeastPriceAndServiceFor(productNames[i],places[i]);
            CourierName bestServForThisCase=choosedService;
            float leastPrice=choosedPrice.getAmount();
                for(int j=0;j<namesSize;j++){
                    if(i==j){continue;}
                    Price price=getEstimationForGivenCourierService(bestServForThisCase,productNames[j],places[j]);
                    leastPrice+=price.getAmount();
                }
            if(priceamount>leastPrice){priceamount=leastPrice;service=bestServForThisCase;}
         }
         choosedPrice=new Price(priceamount);
         choosedService=service;
    }



    public Price getChoosedPrice() {
        return choosedPrice;
    }

    public CourierName getChoosedService() {
        return choosedService;
    }
}
