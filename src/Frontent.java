import java.util.HashMap;

public class Frontent {
    OnlineStore store;


    Frontent(){
        HashMap<CategoryName,CategoryCharger> fedexCategoryCharges=new HashMap<>();
        fedexCategoryCharges.put(CategoryName.ELECTRONICS,new CategoryCharger(0,50,30));
        fedexCategoryCharges.put(CategoryName.FITNESS,new CategoryCharger(10,0,0));
        fedexCategoryCharges.put(CategoryName.HOME_APPLIANCES,new CategoryCharger(0,0,0));

        HashMap<PlaceName,PlaceCharger>fedexPlacesCharges=new HashMap<>();
        fedexPlacesCharges.put(PlaceName.CHENNAI,new PlaceCharger(20));
        fedexPlacesCharges.put(PlaceName.BENGALORE,new PlaceCharger(15));

        HashMap<CategoryName,CategoryCharger> bluedartCategoryCharges=new HashMap<>();
        bluedartCategoryCharges.put(CategoryName.ELECTRONICS,new CategoryCharger(20,0,0));
        bluedartCategoryCharges.put(CategoryName.FITNESS,new CategoryCharger(0,0,0));
        bluedartCategoryCharges.put(CategoryName.HOME_APPLIANCES,new CategoryCharger(0,0,0));

        HashMap<PlaceName,PlaceCharger>bluedartPlacesCharges=new HashMap<>();
        bluedartPlacesCharges.put(PlaceName.CHENNAI,new PlaceCharger(10));
        bluedartPlacesCharges.put(PlaceName.BENGALORE,new PlaceCharger(5));



        Courier fedex=new Courier(CourierName.FEDEX);
        fedex.setCategoryChargers(fedexCategoryCharges);
        fedex.setPlaceChargers(fedexPlacesCharges);

        Courier bluedart=new Courier(CourierName.BLUEDART);
        bluedart.setCategoryChargers(bluedartCategoryCharges);
        bluedart.setPlaceChargers(bluedartPlacesCharges);

        HashMap<CourierName,Courier> couriers=new HashMap<>();
        couriers.put(CourierName.FEDEX,fedex);
        couriers.put(CourierName.BLUEDART,bluedart);

        HashMap<Products,Product> products=new HashMap<>();
        products.put(Products.EARPHONES,new Product(Products.EARPHONES, CategoryName.ELECTRONICS,new Price(300f)));
        products.put(Products.SHOES,new Product(Products.SHOES, CategoryName.FITNESS,new Price(700f)));
        products.put(Products.IRONBOX,new Product(Products.IRONBOX, CategoryName.HOME_APPLIANCES,new Price(1200f)));

        store=new OnlineStore(products,couriers);

    }
    public static void main(String[] args) {
        Frontent frontent=new Frontent();

        Order firstOrder=frontent.store.getOrder(Products.IRONBOX, CourierName.BLUEDART, PlaceName.BENGALORE);
        Price priceOne=firstOrder.getDelivered();

        System.out.println("Charge for delivering ironbox in bluedart to bengalore is "+priceOne.getAmount());


        Order secondOrder=frontent.store.getOrder(Products.SHOES, CourierName.FEDEX, PlaceName.CHENNAI);
        Price secondPrice=secondOrder.getDelivered();

        System.out.println("Charge for delivering shoes in fedex to chennai is "+secondPrice.getAmount());


        ShoppingAid aid=frontent.store.getAid();
        aid.getLeastPriceAndServiceFor(Products.EARPHONES, PlaceName.CHENNAI);

        System.out.println("Suggested service for delivering earphones to chennai is "+aid.getChoosedService()+" for a charge of "+aid.choosedPrice.getAmount());


        Products[] products={Products.SHOES,Products.IRONBOX};
        PlaceName[] places={PlaceName.CHENNAI, PlaceName.BENGALORE};
        aid.getLeastPriceAndServiceFor(products,places);

        System.out.println("Suggested service for delivering shoes to chennai and ironbox to bengalore is "+aid.getChoosedService()+" for a charge of "+aid.choosedPrice.getAmount());


    }
}
